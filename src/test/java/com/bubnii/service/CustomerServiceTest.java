package com.bubnii.service;

import com.bubnii.model.dao.interfaces.CustomerDao;
import com.bubnii.model.entity.Customer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTest {

    @Mock
    private CustomerService customerService;
    @Mock
    private CustomerDao customerDao;

    private List<Customer> customers;

    @Before
    public void init() {
        when(customerDao.findById(1)).thenReturn(Optional.of(createTestEntity(1, "Bob", 19, 500d)));
    }
    @Test
    public void findById() {
        customerService = new CustomerService(customerDao);
        when(customerDao.findById(1)).thenReturn(Optional.of(createTestEntity(1, "Alan", 23, 900d)));
        Optional<Customer> actual = customerService.findById(1);
        assertEquals("Alan", actual.get().getName());
        Mockito.verify(customerDao).findById(1);
    }

    @Test
    public void save() {
        customerService = new CustomerService(customerDao);
        customerService.save(createTestEntity(1,"Bob", 22, 500d));
        Mockito.verify(customerDao).save(createTestEntity(1,"Bob", 22, 500d));
    }

    @Test
    public void update() {
        customerService = new CustomerService(customerDao);
        customerService.update(createTestEntity(4, "Lola", 27, 1800d));
        Mockito.verify(customerDao).update(createTestEntity(4, "Lola", 27, 1800d));
    }

    @Test
    public void delete() {
        customerService = new CustomerService(customerDao);
        customerService.delete(1);
        Mockito.verify(customerDao).delete(1);
    }

    @Test
    public void findAll() {
        customerService = new CustomerService(customerDao);
        when(customerDao.findAll()).thenReturn(Arrays.asList(
                createTestEntity(1, "Tom", 23, 900d),
                createTestEntity(2, "Anna", 19, 500d),
                createTestEntity(3, "Bob", 35, 3600d),
                createTestEntity(4, "Lola", 27, 1800d)));
        List<Customer> actual = customerService.findAll();
        assertEquals(4, actual.size());
        Mockito.verify(customerDao).findAll();
    }

    @Test
    public void findAllByName() {
        customerService = new CustomerService(customerDao);
        when(customerDao.findAllByName("Tom")).thenReturn(Arrays.asList(
                createTestEntity(1, "Tom", 23, 900d),
                createTestEntity(5, "Tom", 19, 500d),
                createTestEntity(6, "Tom", 35, 3600d),
                createTestEntity(9, "Tom", 27, 1800d)));
        List<Customer> actual = customerService.findAllByName("Tom");
        assertEquals("Tom", actual.get(1).getName());
        Mockito.verify(customerDao).findAllByName("Tom");
    }

    private Customer createTestEntity(Integer id, String name, Integer age, Double salary) {
        Customer customer = new Customer();
        customer.setId(id);
        customer.setName(name);
        customer.setAge(age);
        customer.setSalary(salary);
        return customer;
    }
}