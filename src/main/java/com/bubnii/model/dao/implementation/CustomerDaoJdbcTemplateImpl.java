package com.bubnii.model.dao.implementation;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.sql.DataSource;

import com.bubnii.model.dao.interfaces.CustomerDao;
import com.bubnii.model.entity.Customer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class CustomerDaoJdbcTemplateImpl implements CustomerDao {

    private JdbcTemplate template;
    //language=SQL
    private final String SQL_DELETE = "DELETE FROM CUSTOMERS WHERE ID = ?";
    private final String SQL_UPDATE = "UPDATE CUSTOMERS SET NAME = ?, AGE = ?, SALARY = ? WHERE ID = ?";
    private final String SQL_SELECT_ALL = "SELECT * FROM CUSTOMERS";
    private final String SQL_SELECT_FIND_BY_ID = "SELECT * FROM CUSTOMERS WHERE ID = ?";
    private final String SQL_INSERT = "INSERT INTO CUSTOMERS(NAME, AGE, SALARY) values (?,?,?)";
    private final String SQL_SELECT_FIND_BY_NAME = "SELECT * FROM CUSTOMERS WHERE NAME = ?";

    private Map<Integer, Customer> customerMap = new HashMap<>();

    public CustomerDaoJdbcTemplateImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    private RowMapper<Customer> customerRowMapper
            = (ResultSet resultSet, int i) ->{
        Integer id = resultSet.getInt("ID");
        if (!customerMap.containsKey(id)) {
            String name = resultSet.getString("NAME");
            Integer age = resultSet.getInt("AGE");
            Double salary = resultSet.getDouble("SALARY");
            Customer customer = new Customer(id, name, age, salary);
            customerMap.put(id, customer);
        }
        return customerMap.get(id);
    };

    @Override
    public Optional<Customer> findById(Integer id) {
        template.query(SQL_SELECT_FIND_BY_ID, customerRowMapper, id);
        if (customerMap.containsKey(id)) {
            return Optional.of(customerMap.get(id));
        }
        return Optional.empty();
    }

    @Override
    public Integer save(Customer model) {
        return template.update(SQL_INSERT, model.getName(), model.getAge(), model.getSalary());
    }

    @Override
    public void update(Customer model) {
        template.update(SQL_UPDATE, model.getName(), model.getAge(), model.getSalary(), model.getId());
    }

    @Override
    public void delete(Integer id) {
        template.update(SQL_DELETE, id);
    }

    @Override
    public List<Customer> findAll() {
        return template.query(SQL_SELECT_ALL, customerRowMapper);
    }

    @Override
    public List<Customer> findAllByName(String name) {
        return template.query(SQL_SELECT_FIND_BY_NAME, customerRowMapper, name);
    }
}
