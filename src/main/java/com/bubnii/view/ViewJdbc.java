package com.bubnii.view;

import com.bubnii.model.dao.implementation.CustomerDaoImpl;
import com.bubnii.model.dao.interfaces.CustomerDao;
import com.bubnii.model.entity.Customer;
import com.bubnii.service.CustomerService;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ViewJdbc implements GeneralView{
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;
    private CustomerService customerService;

    public ViewJdbc() {
        customerService = new CustomerService(new CustomerDaoImpl(ConnectionUtil.getConnectionUtil()));
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "\t1 - Show all customers");
        menu.put("2", "\t2 - Show customer by id");
        menu.put("3", "\t3 - Show customers by name");
        menu.put("4", "\t4 - Save customer");
        menu.put("5", "\t5 - Update customer");
        menu.put("6", "\t6 - Delete customer");
        menu.put("Q", "\tQ - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::getAllCustomers);
        methodsMenu.put("2", this::getCustomerById);
        methodsMenu.put("3", this::getCustomersByName);
        methodsMenu.put("4", this::saveCustomer);
        methodsMenu.put("5", this::updateCustomer);
        methodsMenu.put("6", this::deleteCustomer);
    }

    private void getAllCustomers() {
        customerService.findAll().forEach(System.out::println);
    }

    private void getCustomerById() {
        System.out.println("Enter the customer id");
        Integer id = input.nextInt();
        customerService.findById(id).ifPresent(System.out::println);
    }

    private void getCustomersByName() {
        System.out.println("Enter the customer name");
        String name = input.next();
        customerService.findAllByName(name).forEach(System.out::println);
    }

    private void saveCustomer() {
        System.out.println("Enter the customer name");
        String name = input.next();
        System.out.println("Enter the customer age");
        Integer age = input.nextInt();
        System.out.println("Enter the customer salary");
        Double salary = input.nextDouble();
        customerService.save(createCustomer(name, age, salary));
    }

    private void updateCustomer() {
        System.out.println("Enter the customer id");
        Integer id = input.nextInt();
        System.out.println("Enter the customer name");
        String name = input.next();
        System.out.println("Enter the customer age");
        Integer age = input.nextInt();
        System.out.println("Enter the customer salary");
        Double salary = input.nextDouble();
        Customer customer = createCustomer(name, age, salary);
        customer.setId(id);
        customerService.update(customer);
    }

    private void deleteCustomer(){
        System.out.println("Enter the customer id");
        Integer id = input.nextInt();
        customerService.delete(id);
    }

    private static Customer createCustomer(String name, Integer age, Double salary) {
        return new Customer(name, age, salary);
    }

    public void show() {
        String keyMenu;
        do {
            System.out.println("------------------------------------------------------------------------------------------");
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.next().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("MENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}
